﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using PetJsonFeed.ServiceLayer;

namespace PetJsonFeed.UnitTests.ServiceLayer
{
    public class JsonServiceTest
    {
        [Test]
        public void GetJsonResponse_Should_Return_Response_When_Url_Is_Passed()
        {
            var jsonService = new JsonService();
            var response= jsonService.GetJsonResponse("http://www.google.com");
            Assert.IsNotEmpty(response);
        }
    }
}
