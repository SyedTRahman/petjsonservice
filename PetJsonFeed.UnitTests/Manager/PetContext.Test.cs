﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using NUnit.Framework.Internal;
using PetJsonFeed.Manager;
using PetJsonFeed.ServiceLayer;

namespace PetJsonFeed.UnitTests.Manager
{
    public class PetContextTest
    {
        private static readonly IJsonService JsonService = Substitute.For<IJsonService>();

        
        [Test]
        public void GetPets_Should_Serialize_When_Not_Empty()
        {
            string data = @"
            [{""name"":""Test"",""gender"":""Male"",""age"":23}]";
            JsonService.GetJsonFeed().Returns(data);
            var petContext = new PetContext(JsonService);

            var list= petContext.GetPets<TestData>();
            Assert.Greater(list.Count(),0);
        }

        [Test]
        public void GetPets_Should_Not_Serialize_When_Empty()
        {
            JsonService.GetJsonFeed().Returns(string.Empty);
            var petContext = new PetContext(JsonService);

            var list = petContext.GetPets<TestData>();
            Assert.AreEqual(list.Count(), 0);
        }

        public class TestData
        {
            public string name { get; set; }
            public string gender { get; set; }
            public int age { get; set; }
        }
    }
}
