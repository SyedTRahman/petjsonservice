﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using PetJsonFeed.BAL;
using PetJsonFeed.Manager;
using PetJsonFeed.Model;

namespace PetJsonFeed.UnitTests.BLL
{
    public class PetJsonServiceTest
    {
        private static readonly IPetContext JsonService = Substitute.For<IPetContext>();

        private IEnumerable<OwnerDetails> SetupData()
        {
            var petList1 = new PetDetails
            {
                PetName = "ABC",
                PetType = "Cat"
            };

            var petList2 = new PetDetails
            {
                PetName = "XYZ",
                PetType = "Cat"
            };

            var petList3 = new PetDetails
            {
                PetName = "DEF",
                PetType = "Dog"
            };

            var petList5 = new PetDetails
            {
                PetName = "ERT",
                PetType = "Cat"
            };

            var petList6 = new PetDetails
            {
                PetName = "GHJ",
                PetType = "Cat"
            };

            var petList7 = new PetDetails
            {
                PetName = "YTR",
                PetType = "Dog"
            };

            List<PetDetails> petData1 = new List<PetDetails> { petList1, petList2, petList3 };
            List<PetDetails> petData2 = new List<PetDetails> { petList5, petList6, petList7 };
            var ownerdata1 = new OwnerDetails
            {
                Gender = "Male",
                Age = 12,
                OwnerName = "Test1",
                PetList = petData1
            };

            var ownerdata2 = new OwnerDetails
            {
                Gender = "FeMale",
                Age = 10,
                OwnerName = "Test1",
                PetList = petData2
            };

            List<OwnerDetails> ownerList = new List<OwnerDetails> { ownerdata1, ownerdata2 };

            return ownerList;
        }

        [Test]
        public void ArrangePets_Should_Sort_Alphabetically()
        {
            IEnumerable<OwnerDetails> listOfOwners = SetupData();
            JsonService.GetPets<OwnerDetails>().Returns(listOfOwners);
            var petContext = new PetJsonService(JsonService);
            var result = petContext.ArrangePets();
            Assert.AreEqual(result[0].PetList[0].PetName, "ABC");
        }

        [Test]
        public void ArrangePets_Should_Group_By_Gender()
        {
            IEnumerable<OwnerDetails> listOfOwners = SetupData();
            JsonService.GetPets<OwnerDetails>().Returns(listOfOwners);
            var petContext = new PetJsonService(JsonService);
            var result = petContext.ArrangePets();
            Assert.AreEqual(result.Count, 2);
        }

       
    }
}
