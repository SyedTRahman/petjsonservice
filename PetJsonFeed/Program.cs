﻿using System;
using Microsoft.Extensions.DependencyInjection;
using PetJsonFeed.BAL;
using PetJsonFeed.Manager;
using PetJsonFeed.ServiceLayer;


namespace PetJsonFeed
{
    public class Program
    {
        static void Main()
        {
            var serviceProvider = ConfigureServices();
            var getPetData = serviceProvider.GetService<IPetJsonService>();
            var listOfCats= getPetData.ArrangePets();

            foreach (var catData in listOfCats)
            {
                Console.WriteLine(catData.Gender);
                foreach (var catDetails in catData.PetList)
                {
                    Console.WriteLine(catDetails.PetName);
                }
            }
            Console.ReadLine();
        }

        private static IServiceProvider ConfigureServices()
        {
            var serviceProvider = new ServiceCollection()
                .AddTransient<IJsonService, JsonService>()
                .AddTransient<IPetContext, PetContext>()
                .AddTransient<IPetJsonService, PetJsonService>()
                .BuildServiceProvider();
            return serviceProvider;
        }

    }
        
}
