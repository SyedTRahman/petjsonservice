﻿using System.Collections.Generic;
using System.Linq;
using PetJsonFeed.Manager;
using PetJsonFeed.Model;

namespace PetJsonFeed.BAL
{

    public interface IPetJsonService
    {
        List<OwnerDetails> ArrangePets();
    }

    public class PetJsonService : IPetJsonService
    {
        private readonly IPetContext _jsonService;

        public PetJsonService(IPetContext jsonService)
        {
            _jsonService = jsonService;
        }

        public List<OwnerDetails> ArrangePets()
        {
            var jsonFeed = _jsonService.GetPets<OwnerDetails>();
            var rootObjects = jsonFeed as OwnerDetails[] ?? jsonFeed.ToArray();
            if (!rootObjects.Any()) return null;
            var list = rootObjects.Where(a => a.PetList != null && a.PetList.Any())
                .GroupBy(a => a.Gender)
                .Select(
                    g =>
                        new OwnerDetails()
                        {
                            Gender = g.Key,
                            PetList =
                                g.SelectMany(l => l.PetList.Where(z => z.PetType.Equals("Cat")))
                                    .OrderBy(x => x.PetName)
                                    .Select(
                                        gg =>
                                            new PetDetails()
                                            {
                                                PetName = gg.PetName

                                            }).ToList()
                        }).ToList();

            return list;

        }

    }
}