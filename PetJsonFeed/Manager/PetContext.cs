﻿using System.Collections.Generic;
using System.Linq;
using PetJsonFeed.ServiceLayer;

namespace PetJsonFeed.Manager
{
    public class PetContext : IPetContext
    {
        private readonly IJsonService _jsonFeed;
        public PetContext(IJsonService jsonFeed)
        {
            _jsonFeed = jsonFeed;
        }

        public IEnumerable<T> GetPets<T>() where T : class
        {
            var petResults = _jsonFeed.GetJsonFeed();
            if (!string.IsNullOrEmpty(petResults))
            {
                var pets = (IEnumerable<T>)Newtonsoft.Json.JsonConvert.DeserializeObject(petResults,
                    typeof(IEnumerable<T>));
                return pets;
            }

            return Enumerable.Empty<T>();
        }
    }
}
