﻿using System.Collections.Generic;

namespace PetJsonFeed.Manager
{
    public interface IPetContext
    {
        IEnumerable<T> GetPets<T>() where T:class ;
    }
}
