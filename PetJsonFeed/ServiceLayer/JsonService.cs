﻿using System.Configuration;
using System.IO;
using System.Net;

namespace PetJsonFeed.ServiceLayer
{
    public class JsonService: IJsonService
    {
        const string ContentType= "application/json";
        public string GetJsonFeed()
        {
            if(ConfigurationManager.AppSettings["JsonFeedUrl"]!=null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["JsonFeedUrl"]))
            {
                var jsonurl = ConfigurationManager.AppSettings["JsonFeedUrl"];
               return GetJsonResponse(jsonurl);
            }
                      
            return string.Empty;
        }

        public string GetJsonResponse(string jsonUrl)
        {
            var webRequest = WebRequest.Create(jsonUrl);
            webRequest.ContentType = ContentType;
            using (var webResponse = webRequest.GetResponse())
            {
                var jsonResults = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
                return jsonResults;
           }
        }

    }
}
