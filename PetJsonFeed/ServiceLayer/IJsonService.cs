﻿namespace PetJsonFeed.ServiceLayer
{
    public interface IJsonService
    {
        string GetJsonFeed();
        string GetJsonResponse(string jsonUrl);
    }
}
