﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PetJsonFeed.Model
{
    public class PetDetails
    {
        [JsonProperty("name")]
        public string PetName { get; set; }
        [JsonProperty("type")]
        public string PetType { get; set; }
    }

    public class OwnerDetails
    {
        [JsonProperty("name")]
        public string OwnerName { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("age")]
        public int Age { get; set; }
        [JsonProperty("pets")]
        public List<PetDetails> PetList { get; set; }
    }

    
}
